(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()(require('angular'))}else if(typeof define==="function"&&define.amd){define(['angular'],f()())}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.NS_Modal = f()(g.angular)}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_MODAL_SIZE = exports.ALERT_CONFIRM_TITLE = exports.ALERT_TITLE = exports.TYPE_ALERT_CONFIRM = exports.TYPE_ALERT = exports.TYPE_MODAL = exports.DEFAULT_ZINDEX = exports.MODAL_CONFIRM_CLASS = exports.MODAL_ALERT_CLASS = exports.SIZE_CLASSES = exports.MODAL_REMOVE_CLASS = exports.MODAL_CONTAINER_CLASS = exports.MODAL_BODY_CLASS = exports.MODAL_BACKDROP_CLASS = exports.HAS_MODAL_CLASS = exports.MODAL_CLASS = exports.PREFIX_CLASS = void 0;

/**
 * @description classes
 */
var PREFIX_CLASS = 'sbj_';
exports.PREFIX_CLASS = PREFIX_CLASS;
var MODAL_CLASS = 'modal';
exports.MODAL_CLASS = MODAL_CLASS;
var HAS_MODAL_CLASS = 'has-modal';
exports.HAS_MODAL_CLASS = HAS_MODAL_CLASS;
var MODAL_BACKDROP_CLASS = 'modal-backdrop';
exports.MODAL_BACKDROP_CLASS = MODAL_BACKDROP_CLASS;
var MODAL_BODY_CLASS = 'modal-body';
exports.MODAL_BODY_CLASS = MODAL_BODY_CLASS;
var MODAL_CONTAINER_CLASS = 'modal-container';
exports.MODAL_CONTAINER_CLASS = MODAL_CONTAINER_CLASS;
var MODAL_REMOVE_CLASS = 'remove';
exports.MODAL_REMOVE_CLASS = MODAL_REMOVE_CLASS;
var SIZE_CLASSES = 'small,medium,large';
exports.SIZE_CLASSES = SIZE_CLASSES;
var MODAL_ALERT_CLASS = 'modal-alert';
exports.MODAL_ALERT_CLASS = MODAL_ALERT_CLASS;
var MODAL_CONFIRM_CLASS = 'modal-confirm';
/**
 * @description default z-index for modal and backdrop
 */

exports.MODAL_CONFIRM_CLASS = MODAL_CONFIRM_CLASS;
var DEFAULT_ZINDEX = 1000;
/**
 * @description modal types
 */

exports.DEFAULT_ZINDEX = DEFAULT_ZINDEX;
var TYPE_MODAL = 1;
exports.TYPE_MODAL = TYPE_MODAL;
var TYPE_ALERT = 2;
exports.TYPE_ALERT = TYPE_ALERT;
var TYPE_ALERT_CONFIRM = 3;
exports.TYPE_ALERT_CONFIRM = TYPE_ALERT_CONFIRM;
var ALERT_TITLE = 'Alert';
exports.ALERT_TITLE = ALERT_TITLE;
var ALERT_CONFIRM_TITLE = 'Confim';
/**
 * @description default size for modal
 */

exports.ALERT_CONFIRM_TITLE = ALERT_CONFIRM_TITLE;
var DEFAULT_MODAL_SIZE = 'medium';
exports.DEFAULT_MODAL_SIZE = DEFAULT_MODAL_SIZE;

},{}],2:[function(_dereq_,module,exports){
"use strict";

/**
 * @author Sinecio bermúdez Jacque
 * @description create constant services
 * @param {Object} mod angular `ns-angularjs.modals` module.
 */
module.exports = function (mod) {
  mod.constant('$modalConstant', _dereq_('./constants'));
};

},{"./constants":1}],3:[function(_dereq_,module,exports){
module.exports = function(){ 
var __$factory__={},angular=arguments[0]; 
 "use strict";

/**
 * @author Sinecio Bermúdez Jacque
 * @description name of angular module. Don't remove or rename the constant `MODULE_NAME`
 * it is used to make the UMD module and so you can do things like the example
 * @example
 *      var angular = angular;
 *      angular.module('myApp', [
 *          require('@ns-angularjs/modal')  //this is the name in package.json
 *      ]);
 */
var MODULE_NAME = 'ns-angularjs.modal';
module.exports = MODULE_NAME;
/**
 * @description Module definition.
 */

var modal = angular.module(MODULE_NAME, []);
/**
 * @description requires other stuffs
 */

_dereq_('./polyfills'); //we always must pass our module


_dereq_('./services')(modal);

_dereq_('./constants')(modal); 
 return MODULE_NAME; }
},{"./constants":2,"./polyfills":5,"./services":6}],4:[function(_dereq_,module,exports){
"use strict";

/**
 * @auhor Sinecio Bermúdez Jacque
 * @description Polyfill Element.remove
 */
if (!('remove' in Element.prototype)) {
  Element.prototype.remove = function () {
    if (!this.parentNode) return;
    this.parentNode.removeChild(this);
  };
}

},{}],5:[function(_dereq_,module,exports){
"use strict";

_dereq_('./element.remove');

},{"./element.remove":4}],6:[function(_dereq_,module,exports){
"use strict";

/**
 * @author Sinecio bermúdez Jacque
 * @description create the main services and providers 
 * @param {Object} mod angular `ns-angularjs.modals` module.
 */
module.exports = function (mod) {
  mod.factory('$modal', _dereq_('./modal-factory')).provider('$modalConfig', _dereq_('./modal-config-provider'));
};

},{"./modal-config-provider":7,"./modal-factory":8}],7:[function(_dereq_,module,exports){
'use strict';

var _constants = _dereq_("../constants/constants");

/**
 * @author Sinecio Bermúdez Jacque
 * @description Provider to config modal service
 */
module.exports = function () {
  /**
   * @description private values;
   */
  var _static = false;
  var _zindex = _constants.DEFAULT_ZINDEX;
  var _alertTitle = _constants.ALERT_TITLE;
  var _confirmTitle = _constants.ALERT_CONFIRM_TITLE;
  var _staticAlert = false;
  var _staticConfirm = false;
  var _alertView = null;
  var _alertUrlView = null;
  var _alertController = null;
  var _modalDefaultSize = _constants.DEFAULT_MODAL_SIZE;
  /**
   * map of all custom alerts
   */

  var _alert = {};
  /**
   * @description functions for config
   */

  this.defaultZindex = setDefaultZindex;
  this.staticModal = setStaticModal;
  this.staticAlert = setStaticAlert;
  this.staticConfirm = setStaticConfirm;
  this.alertTitle = setAlertTitle;
  this.confirmTitle = setConfirmTitle;
  this.alertView = setAlertView;
  this.alertUrlView = setAlertUrlView;
  this.alertController = setAlertController;
  this.alert = setAlert;
  this.defaultModalSize = setDefaultModalSize;
  /**
   * @description Factory
   */

  this.$get = get;
  /**
   * @description sets static = true, so the modals are static by default.
   * clicking on modal backdrop does not fires a close modal
   */

  function setStaticModal() {
    _static = true;
    return this;
  }
  /**
   * @description sets static for all alerts.
   */


  function setStaticAlert() {
    _staticAlert = true;
    return this;
  }
  /**
   * @description sets static for all alerts of type confirm
   */


  function setStaticConfirm() {
    _staticConfirm = true;
    return this;
  }
  /**
   * @description defines a number for z-index
   * @param {Number} zindex default z-index for modals
   */


  function setDefaultZindex(zindex) {
    _zindex = zindex;
    return this;
  }
  /**
   * @description defines a default title for modal alert
   * @param {String} zindex default z-index for modals
   */


  function setAlertTitle(title) {
    _alertTitle = title;
    return this;
  }
  /**
   * @description defines a default title for modal alert of type confirm
   * @param {String} zindex default z-index for modals
   */


  function setConfirmTitle(title) {
    _confirmTitle = title;
    return this;
  }
  /**
   * @description defines a default view for alerts
   * @param {String} view inline view template
   */


  function setAlertView(view) {
    _alertView = view;
    return this;
  }
  /**
   * @description defines a default url view for alerts
   * @param {String} url url to get the view
   */


  function setAlertUrlView(url) {
    _alertUrlView = url;
    return this;
  }
  /**
   * @description defines a default controller for alerts
   * @param {*} ctrl controller for alerts, it can be a function, array or named controller
   */


  function setAlertController(ctrl) {
    _alertController = ctrl;
    return this;
  }
  /**
   * @description defines a new config for custom alert
   * @param {String} name name of alert
   * @param {Object} config literal config object
   */


  function setAlert(name, config) {
    if (!angular.isObject(config)) throw 'Config must be a literal config object';
    _alert[name] = config;
    return this;
  }
  /**
   * @description defines the default modal size
   * @param {String} size name of size to use for modals
   */


  function setDefaultModalSize(size) {
    size = String(size).toLowerCase();
    if (_constants.SIZE_CLASSES.split(',').indexOf(size) < 0) console.warn("size \"".concat(size, "\" is not a valid size: ").concat(_constants.SIZE_CLASSES));
    _modalDefaultSize = size;
    return this;
  }
  /**
   * @description Factory constructor
   */


  function get() {
    return {
      isStatic: isStatic,
      getDefaultZindex: getDefaultZindex,
      getAlertTitle: getAlertTitle,
      getAlertView: getAlertView,
      getAlertUrlView: getAlertUrlView,
      getAlertController: getAlertController,
      getAlert: getAlert,
      getDefaultModalSize: getDefaultModalSize
    };
  }
  /**
   * @description determines if modal is static by default
   * @param {Number} type type of modal.
   */


  function isStatic(type) {
    var values = {};
    values[_constants.TYPE_ALERT] = _staticAlert;
    values[_constants.TYPE_ALERT_CONFIRM] = _staticConfirm;
    return values[type] == null ? _static : values[type];
  }
  /**
   * @description returns the z-index value
   */


  function getDefaultZindex() {
    return _zindex;
  }
  /**
   * @description returns the title for modal alerts
   * @param {Number} type type of modal alert
   */


  function getAlertTitle(type) {
    var titles = {};
    titles[_constants.TYPE_ALERT] = _alertTitle;
    titles[_constants.TYPE_ALERT_CONFIRM] = _confirmTitle;
    return titles[type];
  }
  /**
   * @description returns the alert view
   */


  function getAlertView() {
    return _alertView;
  }
  /**
   * @description returns the alert url view
   */


  function getAlertUrlView() {
    return _alertUrlView;
  }
  /**
   * @description returns the alert controller (for alert and confirm)
   */


  function getAlertController() {
    return _alertController;
  }
  /**
   * @description returns the alert by name
   * @param {String} name name of alert config
   */


  function getAlert(name) {
    return _alert[name] || {};
  }
  /**
   * @description returns the default modal size
   */


  function getDefaultModalSize() {
    return _modalDefaultSize;
  }
};

},{"../constants/constants":1}],8:[function(_dereq_,module,exports){
"use strict";

var _constants = _dereq_("../constants/constants");

module.exports = ['$q', '$controller', '$compile', '$interpolate', '$rootScope', '$http', '$modalConfig', function (q, controller, compile, interpolate, rootScope, http, modalConfig) {
  return {
    openModal: openModal,
    alert: alert,
    confirm: confirm
  };
  /**
   * @author Sinecio Bermúdez Jacque
   * @description opens a modal
   * @param {Object} config plain config object 
   */

  function openModal(config) {
    var def = q.defer();
    if (!config.type) config.type = _constants.TYPE_MODAL;
    getModalView(config).then(function (view) {
      resolve(config).then(function (solved) {
        buildModal(def, view, solved, config);
      });
    }).catch(function (resp) {
      def.reject(resp);
      return resp;
    });
    return def.promise;
  }
  /**
   * @description opens an alert with a single button accept
   * @param {String} message message to show in alert
   * @param {String} name name of custom alert (optional)
   */


  function alert(message, name) {
    return getAlertPromise(message, _constants.TYPE_ALERT, name);
  }
  /**
   * @description opens an alert confirm with accept and cancel buttons
   * @param {String} message message to show in alert confirm
   * @param {String} name name of custom alert (optional)
   */


  function confirm(message, name) {
    return getAlertPromise(message, _constants.TYPE_ALERT_CONFIRM, name);
  }
  /**
   * @description wraps openModal into a promise using a config to modal alerts
   * @param {String} message message to show in alert
   * @param {Number} type type of modal alert
   * @param {String} name name of custom alert (optional)
   */


  function getAlertPromise(message, type, name) {
    var def = q.defer();
    openModal(getAlertConfig(message, type, name)).then(function (resp) {
      def.resolve(resp);
    }).catch(function (resp) {
      def.reject(resp);
    });
    return def.promise;
  }
  /**
   * @description makes a config to modal alert
   * @param {String} message message to show in alert
   * @param {Number} type type of modal alert
   * @param {String} name name of custom alert (optional)
   */


  function getAlertConfig(message, type, name) {
    var config = {
      view: modalConfig.getAlertView(),
      urlView: modalConfig.getAlertUrlView(),
      type: type,
      static: modalConfig.isStatic(type),
      controller: modalConfig.getAlertController() || ['$scope', '$modalInstance', function (scope, modalInstance) {
        scope.$modalConfirmType = _constants.TYPE_ALERT_CONFIRM;

        scope.accept = function () {
          modalInstance.close();
        };

        scope.cancel = function () {
          modalInstance.dismiss();
        };
      }]
    };
    if (name) angular.extend(config, modalConfig.getAlert(name));

    if (type != _constants.TYPE_MODAL) {
      angular.extend(config, {
        message: message,
        title: modalConfig.getAlertTitle(type)
      });
    }

    return config;
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description returns the current modal container, if this does not exist then creates one.
   */


  function getModalContainer() {
    return document.querySelector("body>.".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_CONTAINER_CLASS)) || createModalContainer();
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description returns the current modal backdrop, if this does not exist then creates one.
   */


  function getModalBackdrop() {
    return getModalContainer().querySelector(".".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_BACKDROP_CLASS)) || createModalBackdrop();
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description create a container element for modals
   */


  function createModalContainer() {
    var container = document.createElement('div');
    container.classList.add("".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_CONTAINER_CLASS));
    document.querySelector('body').appendChild(container);
    return container;
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description create a modal backdrop element
   */


  function createModalBackdrop() {
    var backdrop = document.createElement('div');
    backdrop.classList.add("".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_BACKDROP_CLASS));
    getModalContainer().appendChild(backdrop);
    return backdrop;
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description adds `sbj_has-modal` class to body, this indicates that
   * there already is a opened modal.
   */


  function addClassToBody() {
    document.querySelector('body').classList.add("".concat(_constants.PREFIX_CLASS).concat(_constants.HAS_MODAL_CLASS));
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description removes `sbj_has-modal` class from body, this indicates that
   * body do not have a opened modal.
   */


  function removeClassFromBody() {
    if (getModals().length <= 0) document.querySelector('body').classList.remove("".concat(_constants.PREFIX_CLASS).concat(_constants.HAS_MODAL_CLASS));
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description get modal view, if config uses `view` param, then resolves `view` otherwise
   * tries to request with `urlView`
   * @param {Object} config plain config object
   */


  function getModalView(config) {
    var def = q.defer();

    if (config.urlView && !config.view) {
      return http.get(config.urlView).then(function (resp) {
        return resp.data;
      });
    } else if (config.type == _constants.TYPE_MODAL) {
      def.resolve(config.view || '');
    } else {
      def.resolve(config.view || _dereq_('../templates/modal-alert.tmpl'));
    }

    return def.promise;
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description resolves all params in `resolve` attribute from config object, these can be literal values, promise objects or functions
   * @param {Object} config plain config object
   */


  function resolve(config) {
    var def = q.defer(),
        params = {};

    if (angular.isObject(config.resolve)) {
      var promises = [],
          resolves = [];
      angular.forEach(config.resolve, function (value, resolve) {
        resolves.push(resolve);
        promises.push(getPromise(value));
      });
      return q.all(promises).then(function (resp) {
        angular.forEach(resp, function (value, i) {
          params[resolves[i]] = value;
        });
        return params;
      });
    } else {
      def.resolve(params);
    }

    return def.promise;
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description wrappes a value into promise object
   * @param {*} value any value to resolve
   */


  function getPromise(value) {
    if (typeof value == 'function') return q.when(value());else return q.when(value);
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description builds modal component and insert node into DOM
   * @param {Object} defer defer to resolve modal instance
   * @param {Oject} view plain config object
   * @param {Object} solved resolved params from `config.resolve`
   * @param {Object} config plain config object
   */


  function buildModal(defer, view, solved, config) {
    var html = interpolate(_dereq_('../templates/modal.tmpl'))({
      view: view
    });
    var scope,
        compiledView,
        modalInstance,
        params = {};
    scope = rootScope.$new(true);
    scope.$modalType = config.type;
    scope.$modalClass = getClass(config);

    if (scope.$modalType != _constants.TYPE_MODAL) {
      scope.$message = config.message;
      scope.$title = config.title;
    }

    angular.extend(params, config.params);
    angular.extend(params, solved);
    modalInstance = {
      scope: scope,
      params: params,
      close: function close() {
        defer.resolve.apply(null, arguments);
        this.scope.$destroy();
        removeModalViewElement(compiledView);
      },
      dismiss: function dismiss() {
        defer.reject.apply(null, arguments);
        this.scope.$destroy();
        removeModalViewElement(compiledView);
      }
    };

    if (config.controller) {
      controller(config.controller, {
        $scope: scope,
        $modalInstance: modalInstance
      });
    } else {
      console.warn('There is not a defined controller for the current modal');
    }

    compiledView = compile(html)(scope);
    insertModal(compiledView, modalInstance, config);
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description makes a string that contains all classes for modals or alerts in
   * function of config object.
   * @param {Object} config plain config object
   */


  function getClass(config) {
    var sizeClasses = _constants.SIZE_CLASSES.split(','),
        classes = [],
        types = {};

    var size = config.size || (config.type == _constants.TYPE_MODAL ? modalConfig.getDefaultModalSize() : null); //classes by type

    types[_constants.TYPE_ALERT] = "".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_ALERT_CLASS);
    types[_constants.TYPE_ALERT_CONFIRM] = "".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_CONFIRM_CLASS); //default class

    classes.push("".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_CLASS));
    if (size != null) size = size.toLowerCase();
    if (sizeClasses.indexOf(size) >= 0) classes.push(size);else if (size != null) console.warn("size \"".concat(size, "\" is not a valid size: ").concat(_constants.SIZE_CLASSES));
    angular.isString(config.class) && classes.push(config.class);
    types[config.type] && classes.push(types[config.type]);
    return classes.join(' ');
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description inserts compiled view into DOM
   * @param {Object} modalInstance current modal instance object
   * @param {Object} config plain config object
   */


  function insertModal(view, modalInstance, config) {
    var modalContainer = getModalContainer();
    var modalBackdrop = getModalBackdrop();
    var modals = getModals();
    var zIndex = Number(angular.element(modals[modals.length - 1]).css('zIndex')) || modalConfig.getDefaultZindex();
    var isStatic = config.static != null ? config.static : modalConfig.isStatic(config.type);
    addClassToBody();
    modalBackdrop.style.zIndex = ++zIndex;
    view.css('zIndex', ++zIndex); //here we use css function because view is a jqLite element

    if (!isStatic) {
      view.on('click', function (e) {
        if (e.target != this) return;
        if (config.type == _constants.TYPE_ALERT) modalInstance.close();else modalInstance.dismiss();
      });
    }

    angular.element(modalContainer).append(view);
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description returns all modals elements from modal container
   */


  function getModals() {
    return getModalContainer().querySelectorAll(".".concat(_constants.PREFIX_CLASS).concat(_constants.MODAL_CLASS));
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description removes a modal `view` element from DOM
   * @param {Object} view view compiled with current scope
   */


  function removeModalViewElement(view) {
    var elem = view[0];
    elem.addEventListener('webkitAnimationEnd', remove);
    elem.addEventListener('mozAnimationEnd', remove);
    elem.addEventListener('msAnimationEnd', remove);
    elem.addEventListener('oAnimationEnd', remove);
    elem.addEventListener('animationEnd', remove);
    elem.classList.add("".concat(_constants.MODAL_REMOVE_CLASS));

    function remove() {
      elem.remove();
      suitModalBackdrop();
      removeClassFromBody();
    }
  }
  /**
   * @author Sinecio Bermúdez Jacque
   * @description suits the modal backdrop. If there are not more modals into modal container
   * then removes the modal backdrop otherwise sets a new zindex in modal backdrop element.
   */


  function suitModalBackdrop() {
    var modals = getModals();
    if (modals.length <= 0) getModalBackdrop().remove();else getModalBackdrop().style.zIndex = Number(angular.element(modals[modals.length - 1]).css('zIndex')) - 1;
  }
}];

},{"../constants/constants":1,"../templates/modal-alert.tmpl":9,"../templates/modal.tmpl":10}],9:[function(_dereq_,module,exports){
module.exports = "<header><span>{{$title}}</span></header><section>{{$message}}</section><footer><button ng-click=accept()>Accept</button> <button ng-click=cancel() ng-if=\"$modalType == $modalConfirmType\">Cancel</button></footer>";

},{}],10:[function(_dereq_,module,exports){
module.exports = "<div ng-class=$modalClass><div class=sbj_modal-content>{{view}}</div></div>";

},{}]},{},[3])(3)
});
//# sourceMappingURL=modal.js.map
