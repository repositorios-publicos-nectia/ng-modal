exports.writeFactory    = require('./write-factory');
exports.factory         = require('./factory');
exports.factoryPath     = require('./factory-path');
exports.standalone      = require('./standalone');
exports.umd             = require('./umd');
exports.aliases         = require('./aliases');