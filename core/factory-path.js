/**
 * @author Sinecio Bermúdez Jacque
 * @description makes a apropiate factory path
 * @param {String} path main file path to generate factory file.
 */
module.exports = function factoryPath(path){
    return path.replace(/([^\/\\]+)\..+$/, "factory");
}