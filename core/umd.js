var umdConfig = require('../conf/umd.config');

function transform(buffer, encoding, standaloneName){
    var mod = [], def = [], glob = [];
    Object.keys(umdConfig).forEach(function(k, i){
        mod.push(`require('${umdConfig[k]}')`);
        def.push(`'${umdConfig[k]}'`);
        glob.push(`g.${k}`);
    });
    return Buffer.from(buffer
    .toString()
    .replace('{module.exports=f()}', `{module.exports=f()(${mod.join(',')})}`)
    .replace('{define([],f)}', `{define([${def.join(',')}],f()())}`)
    .replace(`g.${standaloneName} = f()`, `g.${standaloneName} = f()(${glob.join(',')})`), encoding);
}

/**
 * @author Sinecio Bermúdez Jacque
 * @description transforms a buffer of code into UMD compatible for CommonJs AMD and Global uses
 */
module.exports = function umd(standaloneName){
    return new require('stream').Transform({
        transform: function (buffer, encoding, cb){
            cb(false, transform(buffer, 'utf8', standaloneName));
        }
    });
};