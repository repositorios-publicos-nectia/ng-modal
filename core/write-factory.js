var fs = require('fs');

/**
 * @author Sinecio Bermúdez Jacque
 * @description writes a factory file to use like entryfile to create a bundle.
 * @param {String} entryFile entry file path
 * @param {String} encoding a valid encoding
 * @param {String} code transpiled code from es6
 */
module.exports = function(entryFile, encoding, code){
    fs.writeFileSync(require('./factory-path')(entryFile), require('./factory')(code), {
        encoding: encoding
    });
}