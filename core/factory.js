var umdConfig = require('../conf/umd.config');

/**
 * @author Sinecio Bermúdez Jacque
 * @description wrapes the code into a factory function to be
 * used into umd build.
 */
module.exports = function factory(code){
    var factory = '__$factory__';
    var mod = [`${factory}={}`];
    Object.keys(umdConfig).forEach(function(k, i){
        mod.push(`${k}=arguments[${i}]`);
        code = code.replace(new RegExp('require\\([\'"]'+umdConfig[k]+'[\'"]\\)','mg'), k);
    });
    code = code.replace(/exports\./g,`${factory}.`);
    return `module.exports = function(){ \nvar ${mod.join(',')}; \n ${code} \n return MODULE_NAME; }`;
};