/**
 * @author Sinecio Bermúdez Jacque
 * @description generates an apropiate exports name for the factory UMD
 */
module.exports = function(exportsName){
    return 'NS_' + exportsName.replace(/(^.)|[-_]./g, function(c) {
        return c.replace(/[^a-z]/ig,'').toUpperCase(c);
    }).replace(/^\d|[^a-z\d]/ig, '');
}