/**
 * @description classes
 */
export const PREFIX_CLASS              = 'sbj_';
export const MODAL_CLASS               = 'modal';
export const HAS_MODAL_CLASS           = 'has-modal';
export const MODAL_BACKDROP_CLASS      = 'modal-backdrop';
export const MODAL_BODY_CLASS          = 'modal-body';
export const MODAL_CONTAINER_CLASS     = 'modal-container';
export const MODAL_REMOVE_CLASS        = 'remove'
export const SIZE_CLASSES              = 'small,medium,large';
export const MODAL_ALERT_CLASS         = 'modal-alert';
export const MODAL_CONFIRM_CLASS       = 'modal-confirm';

/**
 * @description default z-index for modal and backdrop
 */
export const DEFAULT_ZINDEX            = 1000;

/**
 * @description modal types
 */
export const TYPE_MODAL = 1;
export const TYPE_ALERT = 2;
export const TYPE_ALERT_CONFIRM = 3;
export const ALERT_TITLE = 'Alert';
export const ALERT_CONFIRM_TITLE = 'Confim';

/**
 * @description default size for modal
 */
export const DEFAULT_MODAL_SIZE = 'medium';