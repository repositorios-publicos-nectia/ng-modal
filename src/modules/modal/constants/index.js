/**
 * @author Sinecio bermúdez Jacque
 * @description create constant services
 * @param {Object} mod angular `ns-angularjs.modals` module.
 */
module.exports = function(mod){
    mod
    .constant('$modalConstant', require('./constants'));
}