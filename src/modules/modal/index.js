/**
 * @author Sinecio Bermúdez Jacque
 * @description name of angular module. Don't remove or rename the constant `MODULE_NAME`
 * it is used to make the UMD module and so you can do things like the example
 * @example
 *      var angular = require('angular');
 *      angular.module('myApp', [
 *          require('@ns-angularjs/modal')  //this is the name in package.json
 *      ]);
 */
const MODULE_NAME = 'ns-angularjs.modal';
module.exports = MODULE_NAME;

/**
 * @description Module definition.
 */
var modal = angular.module(MODULE_NAME, []);

/**
 * @description requires other stuffs
 */
require('./polyfills');

//we always must pass our module
require('./services')(modal);
require('./constants')(modal);
