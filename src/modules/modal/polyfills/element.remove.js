/**
 * @auhor Sinecio Bermúdez Jacque
 * @description Polyfill Element.remove
 */
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (!this.parentNode) return;
        this.parentNode.removeChild(this);
    };
}