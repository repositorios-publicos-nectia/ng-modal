/**
 * @author Sinecio bermúdez Jacque
 * @description create the main services and providers 
 * @param {Object} mod angular `ns-angularjs.modals` module.
 */
module.exports = function(mod){
    mod
    .factory('$modal', require('./modal-factory'))
    .provider('$modalConfig', require('./modal-config-provider'));
}