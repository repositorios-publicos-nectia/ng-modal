'use strict';

import {
    DEFAULT_ZINDEX,
    TYPE_ALERT,
    TYPE_ALERT_CONFIRM,
    ALERT_TITLE,
    ALERT_CONFIRM_TITLE,
    SIZE_CLASSES,
    DEFAULT_MODAL_SIZE
} from '../constants/constants';

/**
 * @author Sinecio Bermúdez Jacque
 * @description Provider to config modal service
 */
module.exports = function () {

    /**
     * @description private values;
     */
    var _static             = false;
    var _zindex             = DEFAULT_ZINDEX;
    var _alertTitle         = ALERT_TITLE;
    var _confirmTitle       = ALERT_CONFIRM_TITLE;
    var _staticAlert        = false;
    var _staticConfirm      = false;
    var _alertView          = null;
    var _alertUrlView       = null;
    var _alertController    = null;
    var _modalDefaultSize   = DEFAULT_MODAL_SIZE;

    /**
     * map of all custom alerts
     */
    var _alert              = {};

    /**
     * @description functions for config
     */
    this.defaultZindex      = setDefaultZindex;
    this.staticModal        = setStaticModal;
    this.staticAlert        = setStaticAlert;
    this.staticConfirm      = setStaticConfirm;
    this.alertTitle         = setAlertTitle;
    this.confirmTitle       = setConfirmTitle;
    this.alertView          = setAlertView;
    this.alertUrlView       = setAlertUrlView;
    this.alertController    = setAlertController;
    this.alert              = setAlert;
    this.defaultModalSize   = setDefaultModalSize;

    /**
     * @description Factory
     */
    this.$get           = get;

    /**
     * @description sets static = true, so the modals are static by default.
     * clicking on modal backdrop does not fires a close modal
     */
    function setStaticModal(){
        _static = true;
        return this;
    }

    /**
     * @description sets static for all alerts.
     */
    function setStaticAlert(){
        _staticAlert = true;
        return this;
    }

    /**
     * @description sets static for all alerts of type confirm
     */
    function setStaticConfirm(){
        _staticConfirm = true;
        return this;
    }

    /**
     * @description defines a number for z-index
     * @param {Number} zindex default z-index for modals
     */
    function setDefaultZindex(zindex){
        _zindex = zindex;
        return this;
    }

    /**
     * @description defines a default title for modal alert
     * @param {String} zindex default z-index for modals
     */
    function setAlertTitle(title){
        _alertTitle = title;
        return this;
    }

    /**
     * @description defines a default title for modal alert of type confirm
     * @param {String} zindex default z-index for modals
     */
    function setConfirmTitle(title){
        _confirmTitle = title;
        return this;
    }

    /**
     * @description defines a default view for alerts
     * @param {String} view inline view template
     */
    function setAlertView(view){
        _alertView = view;
        return this;
    }

    /**
     * @description defines a default url view for alerts
     * @param {String} url url to get the view
     */
    function setAlertUrlView(url){
        _alertUrlView = url;
        return this;
    }

    /**
     * @description defines a default controller for alerts
     * @param {*} ctrl controller for alerts, it can be a function, array or named controller
     */
    function setAlertController(ctrl){
        _alertController = ctrl;
        return this;
    }

    /**
     * @description defines a new config for custom alert
     * @param {String} name name of alert
     * @param {Object} config literal config object
     */
    function setAlert(name, config){
        if (!angular.isObject(config)) throw 'Config must be a literal config object'
        _alert[name] = config;
        return this;
    }

    /**
     * @description defines the default modal size
     * @param {String} size name of size to use for modals
     */
    function setDefaultModalSize(size){
        size = String(size).toLowerCase();
        if (SIZE_CLASSES.split(',').indexOf(size) < 0) console.warn(`size "${size}" is not a valid size: ${SIZE_CLASSES}`);
        _modalDefaultSize = size;
        return this;
    }

    /**
     * @description Factory constructor
     */
    function get(){
        return {
            isStatic            : isStatic,
            getDefaultZindex    : getDefaultZindex,
            getAlertTitle       : getAlertTitle,
            getAlertView        : getAlertView,
            getAlertUrlView     : getAlertUrlView,
            getAlertController  : getAlertController,
            getAlert            : getAlert,
            getDefaultModalSize : getDefaultModalSize
        };
    }

    /**
     * @description determines if modal is static by default
     * @param {Number} type type of modal.
     */
    function isStatic(type){
        var values = {};
        values[TYPE_ALERT]          = _staticAlert;
        values[TYPE_ALERT_CONFIRM]  = _staticConfirm;
        return values[type] == null ? _static : values[type];
    }

    /**
     * @description returns the z-index value
     */
    function getDefaultZindex(){
        return _zindex;
    }

    /**
     * @description returns the title for modal alerts
     * @param {Number} type type of modal alert
     */
    function getAlertTitle(type){
        var titles = {};
        titles[TYPE_ALERT]          = _alertTitle;
        titles[TYPE_ALERT_CONFIRM]  = _confirmTitle;
        return titles[type];
    }

    /**
     * @description returns the alert view
     */
    function getAlertView(){
        return _alertView;
    }

    /**
     * @description returns the alert url view
     */
    function getAlertUrlView(){
        return _alertUrlView;
    }

    /**
     * @description returns the alert controller (for alert and confirm)
     */
    function getAlertController(){
        return _alertController;
    }
    
    /**
     * @description returns the alert by name
     * @param {String} name name of alert config
     */
    function getAlert(name){
        return _alert[name] || {};
    }

    /**
     * @description returns the default modal size
     */
    function getDefaultModalSize(){
        return _modalDefaultSize;
    }

};