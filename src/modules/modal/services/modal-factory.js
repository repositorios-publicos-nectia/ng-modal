import {
    PREFIX_CLASS,
    HAS_MODAL_CLASS,
    MODAL_CONTAINER_CLASS,
    MODAL_REMOVE_CLASS,
    MODAL_BACKDROP_CLASS,
    MODAL_CLASS,
    SIZE_CLASSES,
    MODAL_ALERT_CLASS,
    MODAL_CONFIRM_CLASS,
    TYPE_ALERT_CONFIRM,
    TYPE_ALERT,
    TYPE_MODAL,
    DEFAULT_MODAL_SIZE
} from '../constants/constants';

module.exports = [

    '$q', '$controller', '$compile', '$interpolate', '$rootScope', '$http', '$modalConfig',

    function(q, controller, compile, interpolate, rootScope, http, modalConfig){

        return {
            openModal: openModal,
            alert    : alert,        
            confirm  : confirm
        };

        /**
         * @author Sinecio Bermúdez Jacque
         * @description opens a modal
         * @param {Object} config plain config object 
         */
        function openModal(config){
            var def = q.defer();
            if (!config.type) config.type = TYPE_MODAL;
            getModalView(config).then(function(view){
                resolve(config).then(function(solved){
                    buildModal(def, view, solved, config);
                });
            }).catch(function(resp){
                def.reject(resp);
                return resp;
            });
            return def.promise;
        }

        /**
         * @description opens an alert with a single button accept
         * @param {String} message message to show in alert
         * @param {String} name name of custom alert (optional)
         */
        function alert(message, name){
            return getAlertPromise(message, TYPE_ALERT, name);
        }

        /**
         * @description opens an alert confirm with accept and cancel buttons
         * @param {String} message message to show in alert confirm
         * @param {String} name name of custom alert (optional)
         */
        function confirm(message, name){
            return getAlertPromise(message, TYPE_ALERT_CONFIRM, name);
        }

        /**
         * @description wraps openModal into a promise using a config to modal alerts
         * @param {String} message message to show in alert
         * @param {Number} type type of modal alert
         * @param {String} name name of custom alert (optional)
         */
        function getAlertPromise(message, type, name){
            var def = q.defer();
            openModal(getAlertConfig(message, type, name)).then(function(resp){
                def.resolve(resp);
            }).catch(function(resp){
                def.reject(resp);
            });
            return def.promise;
        }

        /**
         * @description makes a config to modal alert
         * @param {String} message message to show in alert
         * @param {Number} type type of modal alert
         * @param {String} name name of custom alert (optional)
         */
        function getAlertConfig(message, type, name){
            var config = {
                view: modalConfig.getAlertView(),
                urlView: modalConfig.getAlertUrlView(),
                type: type,
                static: modalConfig.isStatic(type),
                controller: modalConfig.getAlertController() || [
                    '$scope', '$modalInstance', function(scope, modalInstance){
                        scope.$modalConfirmType = TYPE_ALERT_CONFIRM;
                        scope.accept = function(){
                            modalInstance.close();
                        }
                        scope.cancel = function(){
                            modalInstance.dismiss();
                        }
                    }
                ]
            };
            if (name) angular.extend(config, modalConfig.getAlert(name));
            if (type != TYPE_MODAL){
                angular.extend(config, {
                    message : message,
                    title   : modalConfig.getAlertTitle(type)
                });
            }
            return config;
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description returns the current modal container, if this does not exist then creates one.
         */
        function getModalContainer(){
            return document.querySelector(`body>.${PREFIX_CLASS}${MODAL_CONTAINER_CLASS}`) || createModalContainer();
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description returns the current modal backdrop, if this does not exist then creates one.
         */
        function getModalBackdrop(){
            return getModalContainer().querySelector(`.${PREFIX_CLASS}${MODAL_BACKDROP_CLASS}`) || createModalBackdrop();
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description create a container element for modals
         */
        function createModalContainer(){
            var container = document.createElement('div');
            container.classList.add(`${PREFIX_CLASS}${MODAL_CONTAINER_CLASS}`);
            document.querySelector('body').appendChild(container);
            return container;
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description create a modal backdrop element
         */
        function createModalBackdrop(){
            var backdrop = document.createElement('div');
            backdrop.classList.add(`${PREFIX_CLASS}${MODAL_BACKDROP_CLASS}`);
            getModalContainer().appendChild(backdrop);
            return backdrop;
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description adds `sbj_has-modal` class to body, this indicates that
         * there already is a opened modal.
         */
        function addClassToBody(){
            document.querySelector('body').classList.add(`${PREFIX_CLASS}${HAS_MODAL_CLASS}`);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description removes `sbj_has-modal` class from body, this indicates that
         * body do not have a opened modal.
         */
        function removeClassFromBody(){
            if (getModals().length <=0) document.querySelector('body').classList.remove(`${PREFIX_CLASS}${HAS_MODAL_CLASS}`);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description get modal view, if config uses `view` param, then resolves `view` otherwise
         * tries to request with `urlView`
         * @param {Object} config plain config object
         */
        function getModalView(config){
            var def = q.defer();           
            if (config.urlView && !config.view) {
                return http.get(config.urlView).then(function(resp){
                    return resp.data;
                });
            } else if (config.type == TYPE_MODAL) {
                def.resolve(config.view || '');
            } else {
                def.resolve(config.view || require('../templates/modal-alert.tmpl'));
            }
            return def.promise;
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description resolves all params in `resolve` attribute from config object, these can be literal values, promise objects or functions
         * @param {Object} config plain config object
         */
        function resolve(config){
            var def = q.defer(), params = {};
            if (angular.isObject(config.resolve)) {
                var promises = [], resolves = [];
                angular.forEach(config.resolve, function(value, resolve){
                    resolves.push(resolve);
                    promises.push(getPromise(value));
                });
                return q.all(promises).then(function(resp){
                    angular.forEach(resp, function(value, i){
                        params[resolves[i]] = value;
                    });
                    return params;
                });
            } else {
                def.resolve(params);
            }
            return def.promise;
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description wrappes a value into promise object
         * @param {*} value any value to resolve
         */
        function getPromise(value){
            if (typeof value == 'function') return q.when(value());
            else return q.when(value);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description builds modal component and insert node into DOM
         * @param {Object} defer defer to resolve modal instance
         * @param {Oject} view plain config object
         * @param {Object} solved resolved params from `config.resolve`
         * @param {Object} config plain config object
         */
        function buildModal(defer, view, solved, config){
            var html = interpolate(require('../templates/modal.tmpl'))({
                view: view
            });
            var scope, compiledView, modalInstance, params = {};
            
            scope = rootScope.$new(true);
            scope.$modalType = config.type;
            scope.$modalClass = getClass(config);

            if (scope.$modalType != TYPE_MODAL) {
                scope.$message = config.message;
                scope.$title = config.title;
            }

            angular.extend(params, config.params);
            angular.extend(params, solved);

            modalInstance = {
                scope: scope,
                params: params,
                close: function(){
                    defer.resolve.apply(null, arguments);
                    this.scope.$destroy();
                    removeModalViewElement(compiledView);
                },
                dismiss: function(){
                    defer.reject.apply(null, arguments);
                    this.scope.$destroy();
                    removeModalViewElement(compiledView);
                }
            };

            if (config.controller) {
                controller(config.controller, {
                    $scope: scope,
                    $modalInstance: modalInstance
                });
            } else {
                console.warn('There is not a defined controller for the current modal');
            }

            compiledView = compile(html)(scope);

            insertModal(compiledView, modalInstance, config);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description makes a string that contains all classes for modals or alerts in
         * function of config object.
         * @param {Object} config plain config object
         */
        function getClass(config){
            var sizeClasses = SIZE_CLASSES.split(','), classes = [], types = {};
            var size = config.size || (config.type == TYPE_MODAL ? modalConfig.getDefaultModalSize() : null);

            //classes by type
            types[TYPE_ALERT]           = `${PREFIX_CLASS}${MODAL_ALERT_CLASS}`;
            types[TYPE_ALERT_CONFIRM]   = `${PREFIX_CLASS}${MODAL_CONFIRM_CLASS}`;

            //default class
            classes.push(`${PREFIX_CLASS}${MODAL_CLASS}`);

            if (size != null) size = size.toLowerCase();
            if (sizeClasses.indexOf(size) >= 0) classes.push(size);
            else if (size != null) console.warn(`size "${size}" is not a valid size: ${SIZE_CLASSES}`);

            angular.isString(config.class) && classes.push(config.class);
            types[config.type] && classes.push(types[config.type]);

            return classes.join(' ');
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description inserts compiled view into DOM
         * @param {Object} modalInstance current modal instance object
         * @param {Object} config plain config object
         */
        function insertModal(view, modalInstance, config){
            var modalContainer = getModalContainer();
            var modalBackdrop = getModalBackdrop();
            var modals = getModals();
            var zIndex = Number(angular.element(modals[modals.length-1]).css('zIndex')) || modalConfig.getDefaultZindex();
            var isStatic = config.static != null ? config.static : modalConfig.isStatic(config.type);

            addClassToBody();

            modalBackdrop.style.zIndex = ++zIndex;
            view.css('zIndex', ++zIndex);   //here we use css function because view is a jqLite element

            if (!isStatic) {
                view.on('click', function(e){
                    if (e.target != this) return;
                    if (config.type == TYPE_ALERT) modalInstance.close();
                    else modalInstance.dismiss();
                });
            }

            angular.element(modalContainer).append(view);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description returns all modals elements from modal container
         */
        function getModals() {
            return getModalContainer().querySelectorAll(`.${PREFIX_CLASS}${MODAL_CLASS}`);
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description removes a modal `view` element from DOM
         * @param {Object} view view compiled with current scope
         */
        function removeModalViewElement(view){
            var elem = view[0];
            
            elem.addEventListener('webkitAnimationEnd', remove);
            elem.addEventListener('mozAnimationEnd', remove);
            elem.addEventListener('msAnimationEnd', remove);
            elem.addEventListener('oAnimationEnd', remove);
            elem.addEventListener('animationEnd', remove);

            elem.classList.add(`${MODAL_REMOVE_CLASS}`);

            function remove(){
                elem.remove();
                suitModalBackdrop();
                removeClassFromBody();
            }
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description suits the modal backdrop. If there are not more modals into modal container
         * then removes the modal backdrop otherwise sets a new zindex in modal backdrop element.
         */
        function suitModalBackdrop(){
            var modals = getModals();
            if (modals.length <= 0) getModalBackdrop().remove();
            else getModalBackdrop().style.zIndex = Number(angular.element(modals[modals.length-1]).css('zIndex')) - 1;
        }
    }
]