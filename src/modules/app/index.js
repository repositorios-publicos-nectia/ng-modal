/**
 * @author Sinecio Bermúdez Jacque
 * @description main script to develop modal, 
 * include here everything you need to develop.
 */
require('angular');

angular.module('app', [
    require('@uirouter/angularjs'),
    require('../modal')
    //require('../../../dist/js/modals')
])
.config(require('./config'));

require('./controllers');
require('./directives');
