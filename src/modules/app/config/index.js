'use strict';

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description app module configuration
 */
module.exports = [

	'$stateProvider', '$locationProvider', '$urlRouterProvider', '$modalConfigProvider', '$modalConstant',

	function(stateProvider, locationProvider, urlRouterProvider, modalConfigProvider, modalConstant){

        locationProvider
        .hashPrefix('')
        .html5Mode(true);

        stateProvider
        .state('main', {
            abstract: true,
            views: {
                'header@': {
                    templateUrl: 'modules/app/views/header.html'
                }
            }
        })
        .state('main.home', {
            url: '/home',
            views: {
            }
        })
        .state('main.modal', {
            url: '/modal',
            views: {
                'body@': {
                    templateUrl: 'modules/app/views/modal.html',
                    controller: 'modal.controller'
                }
            }
        });

        urlRouterProvider.otherwise('/home');

        modalConfigProvider
        .staticModal()
        .staticAlert()
        .staticConfirm()
        .defaultZindex(3000)
        .alertTitle('Custom Title')
        .confirmTitle('Custom Confirm')
        .alertUrlView('/modules/app/views/modal-custom.html')
        .alertController([
            '$scope', '$modalInstance', '$modalConstant',
            function(scope, modalInstance, modalConstant){
                
                scope.esConfirmar = scope.$modalType == modalConstant.TYPE_ALERT_CONFIRM;
                scope.miTitulo = scope.$title;
                scope.miMensaje = scope.$message;

                scope.aceptar = function(){
                    modalInstance.close();
                }

                scope.cancelar = function(){
                    modalInstance.dismiss();
                }
            }
        ])
        .alert('confirmSINO', {
            urlView: '/modules/app/views/modal-custom-si-no.html',
            static: false,
            type: modalConstant.TYPE_ALERT_CONFIRM,
            controller: [
                '$scope', '$modalInstance',
                function(scope, modalInstance){
                    
                    scope.si = function(){
                        modalInstance.close();
                    }
    
                    scope.no = function(){
                        modalInstance.dismiss();
                    }
    
                }
            ]
        });
	}

];