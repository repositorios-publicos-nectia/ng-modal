module.exports = [
    '$scope','$modal', '$q',
    function(scope, modal, q){

        scope.openModal = openModal;
        scope.callback = callback;
        scope.alert = alert;
        scope.customAlert = customAlert;

        scope.defer = q.defer();

        scope.controller = [
            '$scope', '$modalInstance',
            function(scope, modalInstance){

                scope.testValue = 'a test value for modal content';

                scope.aceptar = function(){
                    modalInstance.close('some for success');
                }

                scope.cancelar = function(){
                    modalInstance.dismiss('some for catch');
                }

            }
        ]

        function customAlert(){
            modal.alert('¿Desea aceptar este dialogo?', 'confirmSINO').then(function(resp){
                console.log('aceptada', resp)
            })
        }

        /**
         * @author Sinecio Bermúdez Jacque
         * @description opens modal using $modal factory
         */
        function openModal(){
            modal.openModal({
                urlView: 'modules/app/views/modal-test.html',
                controller: 'modal.modal-test-controller',
                //controller: scope.controller,
                resolve: {
                    valor1: function(){
                        return 12;
                    },
                    promesa: function(){
                        return getPromesa().then(function(resp){
                            scope.defer.resolve(resp);
                            return resp;
                        });
                    },
                    literal: 'valor literal',
                    prom: scope.defer.promise
                },
                params: {
                    callback: scope.callback
                }
            })
            .then(function(resp){
                console.log(resp);
            }).catch(function(resp){
                console.log(resp);
            });
        }

        function alert(){
            modal.alert('este es el mensaje').then(function(resp){
                console.log('aceptada', resp)
            }).catch(function(resp){
                console.log('cancelada', resp);
            })
        }

        function getPromesa(){
            var def = q.defer();
            setTimeout(function(){
                def.resolve('valor asincrono')
            }, 500);
            return def.promise;
        }

        function callback(){
            console.log('este es el callback');
        }

    }
]