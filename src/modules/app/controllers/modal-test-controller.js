module.exports = [
    '$scope', '$http', '$modalInstance', '$modal', '$q',
    function(scope, http, modalInstance, modal, q){
        scope.testValue = 'a test value for modal content';

        scope.aceptar = function(){
            modalInstance.close('some for success');
        }

        scope.cancelar = function(){
            modalInstance.dismiss('some for catch');
        }

        scope.alert = alert;

        scope.openModal = function(){
            modal.openModal({
                urlView: 'modules/app/views/modal-test.html',
                controller: 'modal.modal-test-controller',
                static: false,
                resolve: {
                    valor1: function(){
                        return 50;
                    },
                    promesa: function(){
                        return getPromesa().then(function(resp){
                            return resp;
                        });
                    },
                    litereal: 'valor literal'
                }
            }).then(function(resp){
                console.log(resp, modalInstance.params.valor1);
            }).catch(function(resp){
                console.log(resp);
            });
        }

        function alert(){
            modal.confirm('este es el mensaje').then(function(resp){
                console.log(resp, 'aceptada');
            }).catch(function(resp){
                console.log(resp, 'cancelada');
            });
        }

        function getPromesa(){
            var def = q.defer();
            setTimeout(function(){
                def.resolve('valor asincrono')
            }, 500);
            return def.promise;
        }

        scope.$on('$destroy', function(e){
            console.log(e);
        })
    }
]