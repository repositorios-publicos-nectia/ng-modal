module.exports = [
    function(){
        return {
            restrict: 'A',
            scope: {
                value: '='
            },
            link: function(scope, elem, attr){
                scope.$testObj = {
                    attr: scope.value
                }
            }
        }
    }
]