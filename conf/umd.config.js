/**
 * @author Sinecio Bermúdez Jacque
 * @description UMD configuration. This file defines all main dependencies for
 * our component, these dependencies won't be include in the factory.
 * The key is the value for global dependency, the value is the commonjs module.
 * To write this file you must read the documentation for each dependency you want
 * exclude from your component at the moment you going to bundle, this because you
 * must know how third party export their components.
 */
module.exports = {
    angular       : 'angular',
}