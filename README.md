# ng-modal
[![Build Status](https://travis-ci.org/SysopNecho/ng-modal.svg?branch=master)](https://travis-ci.org/SysopNecho/ng-modal) [![Build Status](https://img.shields.io/badge/Framework-AngularJS-blue.svg?logo=angular)](https://angularjs.org/) [![Build Status](https://img.shields.io/badge/Style-Sass-pink.svg?logo=sass)](https://sass-lang.com/) 

ng-modal es un servicio que nos permite crear modals sin limites [angularjs](https://angularjs.org/).

## Instalación
```sh
npm install @ns-angularjs/modal --save
```
## Uso

**1.-** Primero que todo debes inyectar la dependencia
```JavaScript
angular.module('myApp', ['ns-angularjs.modal']) 

//o si estás usando CommonJS
angular.module('myApp', [
    require('@ns-angularjs/modal') 
])
```

**2.-** Luego, para renderizar una modal, debes inyectar el servicio `$modal`, hacer uso de la función `openModal` y pasar la configuración:
```JavaScript
angular.module('myApp')
.controller('myController', [
    '$scope', '$modal',
    function(scope, modal){

        scope.openModal = openModal;

        function openModal(){
            modal.openModal({
                urlView: 'modules/modals/myModal.html',
                controller: 'myModal-controller'
            }).then(function(resp){
                /**
                 * cuando modalInstance.close()
                 */
                console.log(resp);
            }).catch(function(resp){
                /**
                 * cuando modalInstance.dismiss()
                 */
                console.log(resp);
            });
        }
    }
])
```
**3.-** Usa `close` o `dismiss` desde el servicio `$modalInstance` para cerrar la modal actual.
ambas funciones cierran la modal, pero se deben usar en distintos casos:

* $modalInstance.close(params) : se utiliza para cerrar la modal con caracter exitoso, en este caso la promesa es resuelta.

* $modalInstance.dismiss(params): se utiliza par cerrar la modal con caracter erroneo o cancelado, en este caso la promesa es cancelada.

```JavaScript
angular.module('myApp')
.controller('myModal-controller', [
    '$scope', '$modalInstance', 
    function(scope, modalInstance){
        
        scope.acceptChanges = acceptChanges;
        scope.cancelChanges = cancelChanges

        function acceptChanges(){
            modalInstance.close('parámetro desde modalInstance.close')
        }

        function cancelChanges(){
            modalInstance.dismiss('parámetro desde modalInstance.dismiss');
        }
    }
])
```

**4.-** También es necesario que importes el estilo básico que da forma y animación a las modales:
```scss
@import '@ns-angularjs/modal/dist/sass/main' //using './node_modules' in sass path
```

Si quieres, además puedes incluir temas que son incluidos:

```scss
@import '@ns-angularjs/modal/dist/sass/main' //using './node_modules' in sass path
@import '@ns-angularjs/modal/dist/sass/themes/light' //using './node_modules' in sass path
```

>Nota:
>Por defecto, se usan las siguientes clases en las modales renderizadas:

> `sbj_has-modal` para el elemento BODY, indica que hay una modal abierta.

> `sbj_modal-container` contenedor para las modales.

> `sbj_modal-backdrop` una capa transparente detrás de las modales.

> `sbj_modal` se utiliza en el componente de modal.

> `sbj_modal-content` envoltorio de tus vistas de modales.

## Objeto de Configuración

* urlView `String` :  url de Vista de Modal o Alerta

* view `String` : plantilla de vista de Modal o Alerta

* controller `Array|Function|String` : controller para Modal, puede ser un array o función con inyección de dependencias, o el nombre de un controller ya declarado.

* static `Boolean` : determina si una Modal o Alerta es estatica, esto significa que si haces click fuera cuando es estatica, esta no será cerrada.

* type `Number` : tipo de Modal o de Alerta, este tipo es tratado internamente, sin embargo puedes utilizarlo para configurar alertas personalizadas, con las constantes `$modalConstant`.

* params `Object` : Objeto literal para informar parámetros desde el controller padre hacia el controller de una Modal

* resolve `Object` : Objeto literal de parámetros que deben ser resueltos antes de instanciar el controller de una Modal.

## $modalConfigProvider

Provider que te permite configurar propiedades por defecto:

* staticModal `Function` : Cualquier Modal será estática por defecto.

* staticAlert `Function` : Cualquier Alerta será estática por defecto. (excepto las Alertas personalizadas).

* staticConfirm `Function` : Cualquier Alerta de tipo confirmar serán estáticas por defecto. (excepto las Alertas personalizadas)

* defaultZindex `Function` : Establece el zindex inicial de las modales.

* alertTitle `Function` : Establece un titulo por defecto para las Alertas. (excepto las Alertas personalizadas).

* confirmTitle `Function` : Establece un titulo por defecto para las Alertas de tipo confirmar. (excepto las Alertas personalizadas).

* alertUrlView `Function` : Establece la url para la vista para las Alertas (de cualquier tipo).

* alertView `Function`: Establece la plantilla para las Alertas (de cualquier tipo).

* alertController `Function`: Establece el controller para las Alertas (de cualquier tipo).

* alert `Function`: Define una nueva alerta Personalizada.

* defaultModalSize `Function`: Define el tamaño por defecto de las modales. (small, medium, large)

## Ejemplo

```JavaScript
angular.module('myApp')
.config([
    '$modalConfigProvider', '$modalConstant',
    function(modalConfigProvider, modalConstant){

        modalConfigProvider
        .staticModal() //modales estaticas
        .defaultZindex(3500) //zindex desde 3500
        .alertTitle('Titulo Personalizado Alerta')
        .confirmTitle('Titulo Personalizado Confirmar')
        .alertUrlView('/views/modals/custom-alert.html')

        /**
         * Este controller puede ser un nombre de controller ya declarado, función o Array con dependencias
         */
        .alertController([
            '$scope', '$modalInstance', '$modalConstant',
            function(scope, modalInstance, modalConstant){
                
                /**
                 * Por defecto el servicio define estas variables
                 */
                console.log(scope.$message);
                console.log(scope.$title);
                console.log(scope.$modalType); 

                /**
                 * podemos escribir cualquier lógica que necesitemos
                 */
                scope.isConfirm = scope.$modalType == modalConstant.TYPE_ALERT_CONFIRM;
                scope.myTitle   = scope.$title;
                scope.myMessage = scope.$message;

                /**
                 * y definir acciones
                 */
                scope.ok = function(){
                    modalInstance.close();
                }
                scope.cancel = function(){
                    modalInstance.dismiss();
                }
            }
        ])

        /**
         * Podemos definir alertas personalizadas. Estas alertas
         * tienen sus propias propiedades aisladas.
         * Podemos abrir una alerta de esta manera:
         * 
         * modal.alert('¿Está seguro?', 'confirmYesNo').then(function(){
         *      console.log('yes');
         * }).catch(function(){
         *      console.log('no');
         * });
         */
        .alert('confirmYesNo', {
            urlView: '/views/modals/confirm-yes-no.html',
            controller: [
                '$scope', '$modalInstance',
                function(scope, modalInstance){

                    /**
                     * Por defecto el servicio define estas variables
                     */
                    console.log(scope.$message);
                    console.log(scope.$title);
                    console.log(scope.$modalType); 
                    
                    scope.yes = function(){
                        modalInstance.close();
                    }
    
                    scope.no = function(){
                        modalInstance.dismiss();
                    }
                }
            ]
        })
    }
])
.controller('myController', [
    '$scope', '$modal', '$q',
    function(scope, modal, q){
        
        scope.openModal = openModal;
        scope.openAlert = openAlert;
        scope.openConfirm = openConfirm;
        scope.openConfirmYesNo = openConfirmYesNo;

        scope.modalController = [
            '$scope', '$modalInstance',
            function(scope, modalInstance){
                scope.myVar = modalInstance.params.promise;
                if (typeof modalInstance.params.callback == 'function'){
                    modalInstance.params.callback('Controller instanciado!')
                }
                scope.closeModal = function(){
                    modalInstance.close({
                        value: 1
                    });
                }
            }
        ];

        function openConfirm(){
            modal.confirm('¿Está seguro?').then(function(){
                console.log('accept');
            }).catch(function(){
                console.log('cancel');
            });
        }

        function openConfirmYesNo(){
            modal.confirm('¿Está seguro?', 'confirmYesNo').then(function(){
                console.log('yes');
            }).catch(function(){
                console.log('no');
            });
        }

        function openModal(){
            var defer = q.defer();
            var config = {
                view: '<div><span>{{myVar}}</span><button ng-click="closeModal">Close</button></div>',
                controller: scope.modalController,
                params: {
                    callback: simpleCallback
                },
                resolve: {
                    Num: 1,
                    Str: "un string",
                    promiseInFunction: function(){
                        var def = q.defer();
                        setTimeout(function(){
                            def.resolve('valor asincrono');
                            defer.resolve('valor resuelto desde promiseInFunction')
                        }, 5000);
                        return def.promise
                    },
                    promise: defer.promise
                    fn: function(){
                        return true;
                    }
                }
            }

            modal.openModal(config).then(function(value){
                console.log(value);
            });
        }

        function simpleCallback(value){
            console.log(value);
        }
    }
])
```
view: `/views/modals/custom-alert.html`
```html
<div>
    <h1>{{myTitle}}</h1>
    <div>
        {{myMessage}}
    </div>
    <div>
        <button ng-click="ok()">Accept</button>
        <button ng-click="cancel()" ng-if="isConfirm">Cancel</button>
    </div>
</div>
```

view:  `/views/modals/confirm-yes-no.html`
```html
<div>
    <h1>{{$title}}</h1>
    <div>
        {{$message}}
    </div>
    <div>
        <button ng-click="yes()">Yes</button>
        <button ng-click="no()">No</button>
    </div>
</div>
```

## Development
Quieres contribuir?
ng-modal usa [Grunt](https://gruntjs.com/) y [Browserify](http://browserify.org/) para desarrollo rápido.
Make a change in your file and instantanously see your updates!
Haz cambios en tus archivos y de inmediato puedes ver la actualización en el navegador

Abre tu terminal y tipea:
```sh
npm install
npm start
```

## License
@ns-angularjs/modal is [MIT licensed](https://github.com/SysopNecho/ng-modal/blob/master/LICENSE.md).